LSOPTS=("--color=auto"
        "--classify"
        "--human-readable"
        "--group-directories-first"
        "--quoting-style=shell"
        "--time-style='+|%m-%d-%y|%H:%M:%S|'")
alias ls="ls ${LSOPTS[*]}"
unset LSOPTS
alias grep='grep -i --color=auto'
alias egrep='egrep -i --color=auto'
alias fgrep='fgrep -i --color=auto'
alias ashow='apt show'
alias c='clear'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias cd-='cd -'
alias cd..='cd ..'
alias du='du -kh'
alias envsort='env | sort'
alias fscat='cat /etc/fstab'
alias g++='g++ -Wall -std=c++11'
alias h='history'
alias hosts='cat /etc/hosts'
alias ins='apt install'
alias j='jobs -l'
alias kk='ll'
alias l11='ls -1sS'
alias l1='ls  -1'
alias ll='ls -l'
alias laa="ls -latur"
alias la="ls  -ltur"
alias lcc="ls -latcr"
alias lc="ls -ltcr"
alias lhh="lll -L"
alias lh="ll  -L"
alias lkk="ls -laSr"
alias lkkr="ls -laS"
alias lkr="ls  -lS"
alias lll="ls -la"
alias l="ls -lA"
alias lm="ls  -ltr"
alias lmm="ls -latr"
alias lmo="ls -Al  | more"
alias lr="ls -lR "
alias lx="ls  -lLXB"
alias lxx="ls -lLaXB"
alias mkdir='mkdir -p'
alias moer='more'
alias moew='more'
alias path='echo -e ${PATH//:/\\n}'
alias ping='ping -c 4 -i 0.5'
alias r='reset'
alias srch='apt search'
alias tree='tree -Csu'
alias trm='--remove-files'
alias vf='cd'
alias vim='vim -c start'
alias v='rvs2 rr'
alias x='exit'
alias xs='cd'
alias g='cd ~'
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias syslog='tail -50 /var/log/syslog'
alias hh='history | sed -r "s/^\s+[0-9]+\s+//g"'
alias pkgsizes='grep-status -FStatus -sInstalled-Size,Package -n '\''install ok installed'\'' -a -FInstalled-Size --gt 0 | paste -sd '\''  \n'\'' | sort -n'
alias pkcontains='apt contains'
alias irgrep='\rgrep -n --color=auto'
alias igrep='\grep --color=auto'
alias iegrep='\egrep --color=auto'
alias lcat='cat -n'
alias mymem='ps -u $USER -o rss --no-headers | xargs python -c "\
import sys;
print \"{:.2f} M\".format(
  sum([ int(i) for i in sys.argv[1:]])/1024.0)
"'
alias agstatp='systemctl status {alphageek,uwsgi}.service'
alias agstat='systemctl status --no-pager {alphageek,uwsgi}.service'
alias scl='systemctl'
alias pgrep='pgrep -laf'
#alias df='df -h'
alias df='df -hx tmpfs -x devtmpfs'
alias lsblk='lsblk -o NAME,RM,RO,TYPE,SIZE,FSTYPE,MOUNTPOINT'
alias ufwstats='ufw status verbose'
alias rgrep='rgrep --color -i'
alias lip='lsof -i -P'
alias ip6='ip -6'