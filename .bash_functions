#!/bin/bash

cl() { cd $1 && ls; }
mkcd() { mkdir -p $1; cd $1; }
bigfs() { du -hax $1 | sort -rh | head -25; }
aptless() { apt search $1 | more; }

aget() {
    declare -n array_name=$1;
    declare -i i=0;
    for ((i=0; i<${#array_name[@]}; i++))
    do
        printf "%q" "${array_name[$i]}";
        (( $i != ${#array_name[@]}-1 )) && printf " ";
    done
}

aprint() {
    declare -n array_name=$1;
    if [ ! -z "$2" ]; then
        declare -i idx=$2;
        declare -i cur=${#array_name[@]};
        declare -i max=$((cur-1));
        if (($idx<=max)) && (($idx>=-$cur)); then
            echo "${array_name[$idx]}";
        else
            echo "index out of range:  -${cur} >= index <= ${max}";
        fi;
    else
        declare -i i=0;
        printf "%s=(\n " "$1";
        for ((i=0; i<${#array_name[@]}; i++))
        do
            printf "[%-d]='%s'\n" "$i" "${array_name[$i]}";
            (( $i != ${#array_name[@]}-1 )) && printf " ";
        done;
        printf ")\n";
    fi
}

aptlog()
{
    local LOGFILE='/var/log/apt/history.log'

    (($# != 0)) && true ||
        eval 'egrep -B 1 '^Command' $LOGFILE && return 0'

    case "$1" in
    'i')
        egrep -B 1 '^Commandline: [._[:alnum:]-]+ install' $LOGFILE
        ;;
    'r')
        egrep -B 1 '^Commandline: [._[:alnum:]-]+ remove'  $LOGFILE
        ;;
    'p')
        egrep -B 1 '^Commandline: [._[:alnum:]-]+ remove --purge' $LOGFILE
        ;;
      *)
        echo "$FUNCNAME: usage: $FUNCNAME [i,r,p]" >&2
        return 2
        ;;
    esac
}

aliad()
{

    (($# < 2)) && {
        echo "usage: $FUNCNAME name [value]"
        return 1
    }

    declare ali="$1"
    shift
    eval alias "${ali}"='$@'

    grep "^alias\s${ali}=" $HOME/.bash_aliases && {
        declare REPLACE=n
        echo "alias $ali already exists."
        read -p "replace? [y/N]: " REPLACE
        [[ "$REPLACE" =~ y|Y ]] || return 3
    }

    declare DOIT=n
    alias "${ali}"
    read  -p "Commit file? [Y/n]: " DOIT

    [[ "$DOIT" =~ n|N ]] && return 4 || {
        sed -i -r "s/^(alias\s${ali}=.*)$/#\1/g"  $HOME/.bash_aliases
        alias "$ali" >> $HOME/.bash_aliases
        . $HOME/.bash_aliases
    }
}

mkchown()
{
    sudo mkdir "$1" &&
        sudo chown "${USER}":"${USER}" "$1"
}

func()
{
    if (($# == 0)) ; then
        echo "usage: func name [name...]" 1>&2
        return 1
    fi

    declare -i local status=0

    for f; do
        if [ "$(builtin type -type $f)" != "function" ] ; then
            echo "func: $f: not a function" 1>&2
            status=1 && continue
        fi
        builtin type $f | sed 1d
    done

    return $status
}

stashadd()
{
    local msg="Commit from $HOSTNAME"
    (($#==1)) && msg="$1"
    echo "git add --all" && git add --all
    (($?==0)) || return $?
    echo "git commit -am '${msg}'" && git commit -am "'${msg}'"
    (($?==0)) || return $?
    echo "git push" && git push
    (($?==0)) || return $?
}

disable-upstart-job()
{
    (($# == 1)) && declare local JOB UPSTART_JOB_CONF OVERRIDE_FILE ||
        eval 'echo Usage: disable-upstart-job JOB && return 1'

    JOB="$(basename "$1")"
    UPSTART_JOB_CONF="/etc/init/"$JOB".conf"
    OVERRIDE_FILE="/etc/init/"$JOB".override"

    if [ -f "$OVERRIDE_FILE" ] && [ "$(cat "$OVERRIDE_FILE" | wc -l)" == 1 ] && grep -q 'manual' "$OVERRIDE_FILE" ; then
        echo "Override file "$OVERRIDE_FILE" already exists." && return 2
    elif [ ! -f "$UPSTART_JOB_CONF" ]; then
        echo "Error: Couldn't find the upstart script "$UPSTART_JOB_CONF"" && return 3
    elif ! initctl list | grep -q "$JOB"; then
        echo "Error: job "$JOB" not known to initctl" && return 4
    else
        sudo sh -c "echo 'manual' > "$OVERRIDE_FILE"" && return 0
    fi
}

detatch()
{
    (($# < 1)) && return 11
    local cmd="$1"

    if (($# == 1)); then
        nohup "$cmd" > /dev/null 2>&1 &
    else
        local opt="$2"
        nohup "$cmd" "$opt" > /dev/null 2>&1 &
    fi
}

arrpush()
{
    eval declare -i local cnt='${#'$1'[*]}'
    eval "$1"[$cnt]='$2'
}

arrput()
{
    if [[ $2 =~ ^[0-9].*$ ]]; then
        eval declare -i local num_elements='${#'"$1[*]}"''
        if (($2 <= $num_elements)); then
             eval "$1"[$2]='$3'
        else
            echo "arrput: index not incremental."
        fi
    else
        echo "Usage: arrput array index value"
    fi
}

awkcols() {
    local infile=${@:$#}
    local cols=${@:1:(($#-1))}

    if [ ! -f ${@:$#} ]; then
        infile=/dev/stdin
        cols=${@:$#}
    fi

    local args=$(echo $cols | perl -pe 's/([[:digit:]])/\$$1/g')
    awk "{print $args}" $infile
}

hput() {
    eval "$1""$2"='$3'
}

hget() {
    if (($#==1)); then
        eval keysarr=('${!'$1[*]}'')
        for key in ${keysarr[*]}; do
            echo -n "$key  :  "
            eval echo '${'$1[$key]}''
        done
    else
        eval echo '${'"$1[$2]"'}'
    fi
}

aget() {
    declare -n array_name=$1;
    declare -i i=0;
    for ((i=0; i<${#array_name[@]}; i++)); do
        printf "%q" "${array_name[$i]}";
        (( $i != ${#array_name[@]}-1 )) && printf " ";
    done
}

ipgrep() {
    grep-status \
        -P -e "$1" \
        -a -FStatus 'install ok installed' \
        -n -s Package | sort
}

funcs() {
    set | egrep -o "^${1}(\w|_)+ "
}

inspkgs() {
    grep-status -FPackage -e "$1" \
        -a -FStatus 'install ok installed' \
        -n -s Package | sort
}

optpkgs() {
    grep-status -FPackage -e "$1" \
        -a -FStatus 'install ok installed' \
        -a -FPriority 'optional' \
        -n -s Package | sort
}

pkgpri() {
    grep-status -FPriority -e "^${1}$" \
        -a -FStatus 'install ok installed' \
        -n -s Package | sort
}

hgrep() {
    case "$1" in
    -n) shift; history | grep "$@" ;;
    -u) shift; history | grep "$@" | sed -r 's/^\s[[:digit:]]+\s{2}//g' | sort | uniq | grep "$@" ;;
     *) history | grep "$@" | sed -r 's/^\s[[:digit:]]+\s{2}//g' | grep "$@" ;;
    esac
}

psu ()
{ 
    ps -u ${1:-$USER} -o pid,%cpu,tty,cputime,cmd
}
