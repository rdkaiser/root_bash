# ~/.profile: executed by Bourne-compatible login shells.

test -d ~/bin && PATH=$HOME/bin:$PATH

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n || true
